# Dynastar Tango-Controls device server

Tango-Controls device server which can create attributes dynamically and automatically based on some device properties:
- dynamic_attributes property can be used to list the names of the dynamic attributes to be created.
- nb_auto_generated_dyn_attr can be used to define the number of additional auto-generated attributes. 
if nb_auto_generated_dyn_attr is set to _n_, the created dynamic attributes will have names like "value0", "value1", ..., "value_n_"

This device server can be used to test/benchmark Tango-Controls kernel.
